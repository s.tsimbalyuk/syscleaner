package dev.tsvinc.syscleaner;

import dev.tsvinc.syscleaner.Util.OS;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.ProcessBuilder.Redirect;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.gradle.tooling.GradleConnectionException;
import org.gradle.tooling.GradleConnector;
import org.gradle.tooling.ProjectConnection;
import org.gradle.tooling.ResultHandler;
import org.tinylog.Logger;

public class Main {

  public static final String MVN = locateMaven()
      .orElseThrow(() -> new RuntimeException("Maven not found"));
  private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("message");
  private static final String BUILD_SUCCESS = "BUILD\\sSUCCESS";
  private static final String FAILED_TO_CLEAN = "failed to clean";
  private static final String ERROR = Main.BUNDLE.getString("error_with_placeholder");
  private static final String STARTED = Main.BUNDLE.getString("started");
  private static final String ARGS_EXPECTED = Main.BUNDLE.getString("arguments_expected");
  private static final Pattern PATTERN_BUILD_SUCCESS = Pattern.compile(Main.BUILD_SUCCESS);
  private static final Set<String> completed = new HashSet<>(100);
  private static final Set<String> failed = new HashSet<>(100);
  private static final List<CompletableFuture<?>> futureList = new ArrayList<>();
  private static final AtomicLong successfulFuturesCounter = new AtomicLong(0);
  private static final ExecutorService executorService = Executors.newSingleThreadExecutor();

  public static void main(final String[] args) {
    Logger.info(Main.STARTED);

    String pathToSearch;

    if (0 < args.length) {
      pathToSearch = args[0];
    } else {
      throw new ArgumentsExpected(Main.ARGS_EXPECTED);
    }
    try {
      final var formattedString = String.format("working directory: %s", pathToSearch);
      Logger.info(formattedString);
      listFilesUsingFileWalkAndVisitor(pathToSearch);
    } catch (final Exception e) {
      Logger.error(e::toString);
    }
    while (!futureList.isEmpty()) {
      futureList.removeIf(CompletableFuture::isDone);
    }
    Logger.info("Failed: {}", failed);
    Logger.info("Completed: {}", completed);
    Logger.info("Done. shutting down. Total processed: {}", successfulFuturesCounter.get());
    executorService.shutdown();
  }


  private static void cleanMavenProject(final Path path) {
    Logger.info(() -> String.format("running maven clean for path: %s", path));
    var builder = new ProcessBuilder();
    try {
      builder = new ProcessBuilder(Main.MVN, "clean", "-f", path.toString());
    } catch (final Exception e) {
      Logger.error(() -> Main.ERROR + e);
    }
    runShellCommand(path.toString(), builder);
  }

  public static Optional<String> locateMaven() {
    final OS os = Util.getOS();
    Optional<String> mavenPath = Optional.empty();
    switch (os) {
      case MAC:
      case LINUX:
        var builder = new ProcessBuilder();
        try {
          builder = new ProcessBuilder("which", "mvn");
        } catch (final Exception e) {
          Logger.error(() -> Main.ERROR + e);
        }
        mavenPath = runShellCommand(builder).stream().findFirst();
        break;
      case WINDOWS:
        var builder1 = new ProcessBuilder();
        try {
          builder1 = new ProcessBuilder("where", "mvn");
        } catch (final Exception e) {
          Logger.error(() -> Main.ERROR + e);
        }
        mavenPath = runShellCommand(builder1).stream()
            .filter(s -> s.endsWith("bat") || s.endsWith("cmd"))
            .findFirst();
        break;
    }
    return mavenPath;
  }

  private static void runShellCommand(final String path, final ProcessBuilder builder) {
    builder.inheritIO().redirectOutput(Redirect.PIPE);
    try {
      final var p = builder.start();
      try (final var buf = new BufferedReader(new InputStreamReader(p.getInputStream()))) {
        String line;
        var status = false;
        while (null != (line = buf.readLine())) {
          if (Main.PATTERN_BUILD_SUCCESS.matcher(line).matches()) {
            status = !status;
          }
        }
        if (status) {
          completed.add(path);
          successfulFuturesCounter.getAndAdd(1);
        } else {
          failed.add(path);
        }
      } catch (final Exception e) {
        p.destroyForcibly();
        Logger.error(() -> Main.ERROR + e);
      }
      p.waitFor();
      p.destroy();
    } catch (IOException | InterruptedException e) {
      Logger.error(() -> Main.ERROR + e);
    }
  }

  /*TODO: Convert to supplier https://www.baeldung.com/java-stream-operated-upon-or-closed-exception#solution*/
  private static List<String> runShellCommand(final ProcessBuilder builder) {
    List<String> list = Collections.emptyList();
    builder.inheritIO().redirectOutput(Redirect.PIPE);
    try {
      final Process p = builder.start();
      try (final var buf = new BufferedReader(new InputStreamReader(p.getInputStream()))) {
        list = buf.lines().collect(Collectors.toList());
      } catch (final Exception e) {
        p.destroyForcibly();
        Logger.error(() -> Main.ERROR + e);
      }
      p.waitFor();
      p.destroy();
    } catch (IOException | InterruptedException e) {
      Logger.error(() -> Main.ERROR + e);
    }

    return list;
  }

  public static void listFilesUsingFileWalkAndVisitor(String dir) {
    try {
      FileVisitor<Path> visitor = new SimpleFileVisitor<>() {
        @Override
        public FileVisitResult visitFile(Path path, BasicFileAttributes attrs) {
          final String fileName = path.getFileName().toString();
          if ("pom.xml".equals(fileName) && !path.toString().contains("classes")) {
            Logger.info(String.format("found pom.xml: %s", path));
            futureList
                .add(
                    CompletableFuture.runAsync(() -> cleanMavenProject(path), executorService)
                        .whenComplete(
                            (unused, throwable) -> successfulFuturesCounter.getAndAdd(1)));
          } else if ("build.gradle".equals(fileName)) {
            Logger.info(String.format("found gradle.build: %s", path));
            futureList
                .add(CompletableFuture.runAsync(() -> cleanGradleProject(path), executorService)
                    .whenComplete((unused, throwable) -> successfulFuturesCounter.getAndAdd(1)));
          } else if ("package.json".equals(path.getFileName().toString())) {
            final Path nodeModules = Path.of(path.getParent().toString(), "node_modules");
            if (Files.isDirectory(nodeModules) && !Pattern.compile(".*node_modules.*node_modules.*")
                .matcher(nodeModules.toString()).matches()) {
              Logger.info(String.format("found node_modules: %s", nodeModules));
              futureList
                  .add(CompletableFuture
                      .runAsync(() -> cleanNodeProject(nodeModules), executorService)
                  );

            }
          }
          return FileVisitResult.CONTINUE;
        }
      };
      Files.walkFileTree(Path.of(dir), visitor);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private static void cleanNodeProject(Path path) {
    Logger.info(() -> "Cleaning node project: {}" + path.toString());
    try {
      Files.walk(path)
          .sorted(Comparator.reverseOrder())
          .forEach(file -> {
            try {
              Files.delete(file);
            } catch (IOException e) {
              Logger.error(() -> String
                  .format("[CLEANING_NODE_PROJECT] Failed to delete: %s, root: %s", file, path));
            }
          });
      completed.add(path.toString());
      successfulFuturesCounter.getAndAdd(1);
    } catch (IOException e) {
      Logger.error(String.format("Failed to clean node project: %s", path));
      failed.add(path.toString());
      e.printStackTrace();
    }
  }

  private static void cleanGradleProject(Path path) {
    Logger.info(() -> "Cleaning gradle project: " + path.toString());
    GradleConnector connector = GradleConnector.newConnector();
    connector.forProjectDirectory(path.getParent().toFile());
    connector.useBuildDistribution();
//    connector.useGradleVersion("7.0");
    try (ProjectConnection connection = connector.connect()) {
      connection.newBuild().forTasks("clean").addArguments("--exclude-task", "test").run(
          new ResultHandler<>() {
            @Override
            public void onComplete(Void result) {
              Logger.info(String.format("cleaned gradle project: %s", path.getParent().toString()));
              completed.add(path.toString());
            }

            @Override
            public void onFailure(GradleConnectionException failure) {
              Logger.error(() -> String.format("%s gradle project: %s. Reason: %s", FAILED_TO_CLEAN, path.getParent().toString(), failure.getMessage()));
              failure.printStackTrace();
              failed.add(path.toString());
            }
          });
    }
  }
}
