package dev.tsvinc.syscleaner;

public class ArgumentsExpected extends RuntimeException {
  private static final long serialVersionUID = 1L;

  public ArgumentsExpected() {}

  public ArgumentsExpected(final String message) {
    super(message);
  }

  public ArgumentsExpected(final String message, final Throwable cause) {
    super(message, cause);
  }

  public ArgumentsExpected(final Throwable cause) {
    super(cause);
  }

  public ArgumentsExpected(
      final String message,
      final Throwable cause,
      final boolean enableSuppression,
      final boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
