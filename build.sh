#!/usr/bin/env bash

BUILD_DIR="build"
if [[ -d "$BUILD_DIR" ]]; then
  rm -rf build
  mkdir -p ${BUILD_DIR}
else
  mkdir -p ${BUILD_DIR}
fi

set -e
#set -x

#GRAAL=/home/tsv/.sdkman/candidates/java/19.0.0-grl/bin
GRAAL=/home/tsv/.sdkman/candidates/java/20.0.0.r11-grl/bin
#UPX=/home/tsv/app/upx-3.95-amd64_linux

mvn clean compile
CP=$(mvn -q exec:exec -Dexec.executable=echo -Dexec.args="%classpath")

cd ${BUILD_DIR}

echo
echo Graal
echo

${GRAAL}/native-image -cp "$CP" \
  -cp "$CP" dev.tsvinc.syscleaner.Main \
  --no-fallback \
  -H:IncludeResourceBundles=message

ls -lh dev.tsvinc.syscleaner.main

#echo
#echo Upx
#echo

#${UPX}/upx dev.tsvinc.syscleaner.main -osyscleaner
#ls -lh syscleaner
